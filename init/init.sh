#!/bin/sh

if [ ! -f "/etc/issue.net" ] || [ "$(cat /etc/issue.net | grep "Ubuntu 14.04" | wc -l)" -ne 1 ]
then
	echo "Not supported environment"
	exit 1
fi

clear

echo "\n  Wellcome!"
echo "  We will install a mapnik render server for offline tile rendering.\n"

while true;
do
	echo "Where do you want to install?"
	echo " (1) /opt"
	echo " (2) $HOME"
	echo -n "Number of the target: "
	
	read TARGET
	
	case $TARGET in
	    1|2) break ;;
	    *) 	clear; continue ;;
	esac
done

if [ $TARGET -eq 1 ];
then
	TARGET="/opt"
else
	TARGET="$HOME"
fi

MAPNIK_DIR="$TARGET/mapnik"
INSTALLER_DIR="$MAPNIK_DIR/init"
VENDOR_DIR="$MAPNIK_DIR/vendor"
TEMP_DIR="$MAPNIK_DIR/tmp"
FUNCTIONS_URL="https://bitbucket.org/connorhu/osmtilerenderserver/raw/f69a5135dab3f24b851555d251d40a79c3eb0f7e/functions.sh"
CONFIG_URL="https://bitbucket.org/connorhu/osmtilerenderserver/raw/f69a5135dab3f24b851555d251d40a79c3eb0f7e/config.sh"

# if [ -d $MAPNIK_DIR ]
# then
# 	echo "$MAPNIK_DIR directory exists. exit."
# 	exit 1
# fi

if [ ! -w $TARGET ]
then
	sudo mkdir -p $MAPNIK_DIR
	sudo chown $(id -u -n):$(id -g -n) $MAPNIK_DIR
else
	mkdir -p $MAPNIK_DIR
fi

if [ ! -w "$MAPNIK_DIR" ]
then
	echo "$MAPNIK_DIR not writeable... :("
	exit 1
fi

cd $MAPNIK_DIR

echo "Install git."
sudo apt-get install -y git

echo "Clone OSMTileRenderServer repo"
git clone https://connorhu@bitbucket.org/connorhu/osmtilerenderserver.git .

. init/step2.sh

