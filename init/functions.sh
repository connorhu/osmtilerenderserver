#!/bin/sh

systemType()
{
	if [ $(file /usr/bin/file | awk '{ print $3 }') = "32-bit" ]
	then
		echo 32
	else
		echo 64
	fi
}

download()
{
	local URL=$1
	local TARGETFILE=$2
	wget -q -O $TARGETFILE $URL
}

installPackages()
{
	sudo apt-get install -y $@
}