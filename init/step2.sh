#!/bn/sh

. $INSTALLER_DIR/functions.sh
. $INSTALLER_DIR/config.sh

installPackages "osm2pgsql" "postgis"

echo "Downloading node"
if [ $(systemType) -eq 32 ]
then
	download $NODE32_URL $TEMP_DIR/node.tar.gz
else
	download $NODE64_URL $TEMP_DIR/node.tar.gz
fi

tar -xf $TEMP_DIR/node.tar.gz -C $TEMP_DIR
mv node-* $VENDOR_DIR/node
rm $TEMP_DIR/node.tar.gz

NODE_BIN_DIR="$VENDOR_DIR/node/bin"
NODE="$NODE_BIN_DIR/node"
NPM="$NODE_BIN_DIR/npm"

echo "todo..."


